<center>
    <img src="https://gitlab.com/ibm/skills-network/courses/placeholder101/-/raw/master/labs/module%201/images/IDSNlogo.png" width="300" alt="cognitiveclass.ai logo"  />
</center>

<link href="style/instruction_style.css" rel="stylesheet"/>

# Peer Review Assignment - Developers



Estimated time needed: **90** minutes



## Objectives

In this assignment you will:

- Populate the Cloudant DB
- Create API endpoints with Cloud Functions
- Create Watson Assistant that uses the API end points from the cloud function and the web application. 
- Integrate the Watson assitant to the website

### Prerequisites - 
1. IBM Cloud Account
2. IBM Watson Assistant instance
3. IBM Cloudant Instance.

## __Task1:__ Populate data into the cloudant db.

> *To run the commands in Cloud Shell, you can type or simply copy and paste in you IBM Cloud shell*

1. Open a new cloud shell. 
<img src="images/1-cloud-shell.png"  class="image_centre_75"/>

2. The IBM Cloud Shell terminal will be initialized. <img src="images/2-cloud-shell-terminal.png"  class=/>

    >This shell timesout when there is no activity. Any changes made will be lost.  

3. Run the following command to clone the repository. 

    ```
    [ ! -d 'tilsj-petshop_webapp' ] && git clone https://github.com/ibm-developer-skills-network/tilsj-petshop_webapp.git
    ```

4. Change to the `tilsj-petshop_webapp` folder. 

    ```
    cd tilsj-petshop_webapp
    ```

5. Install the packages required for the project  by running the following command

    ```
    pip3 install -r requirements.txt
    ```

6. Create a file `.env` under the current directory to map the apikey and username of the cloudant service noted in the pre-work session. 
    - Type `nano .env`. 
    - Add the credenetials as mentioned below. 
        ```
        account_name=<your username>
        apikey=<your apikey>
        ```
    - Once done, press `Ctrl+o` and enter to save. 
    - Press `Ctrl+x` to exit the nano editor. 
    <img src="images/env_nano_editor.png"  style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>
    
 
7. Run the following script to to populate the database. 
*This script connects to the Cloudant DB using the credentials provided in the .env file and populates data in it.*

    ```
    python3 addObjectsToCloudant.py
    ```
{: codeblock}
>This takes a few seconds. 

8. Check the cloudant database for the new documents you just added.
<img src="images/launch_db_dashboard.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

    *You should see a database named `pets-database` with 15 docs created.*

<img src="images/pets_database.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

> <span title="camera">📷</span> **Take a screenshot of the Cloudant Database dashboard showing pets-database with 15 docs and save it as **'pets-database'** in .jpg or .png format for the peer-graded assignment.**


### Task 2 - API endpoints with Cloud Functions

1. On the IBM cloud [homepage](https://cloud.ibm.com/), go to the menu on the left and choose `functions` and then choose `Actions`.

2. Enter the name for the function, choose `python 3.7` for the runtime type and click on `Create`.

<img src="images/function_name.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

3. Remove the existing default content and paste the following code in the code space provided. Replace the cloud username and apikey with your credentials. 

```python
from cloudant.client import Cloudant
from cloudant.error import CloudantException
from cloudant.result import Result, ResultByKey
from cloudant.document import Document


def main(dict):
    databaseName = "pets-database"
    account_user_name = "<replace with your cloudant username>"
    apikey="replace with your cloudant apikey"
    client=Cloudant.iam(account_name=account_user_name, api_key=apikey, connect=True)
    myDatabase = client.create_database(databaseName)
    req = dict['req']
    print(req)
    if (req == "getAllDetails"):
        kind = dict['kind']
        selector = {'kind': {'$eq': kind}}
        docs = myDatabase.get_query_result(selector)
        options = []
        for doc in docs:
            pet_instance ={"label": str(doc["name"]+ " " + doc["age"] + " "+ doc["breed"]),"value": {"input":{"text":doc["_id"]}}}
            options.append(pet_instance)
    
        return { "generic":[
                    {"title":"Please choose one of these pets",
                    "options":options,
                            "description":"",
                            "response_type":"option"
                    }]}
    elif (req == "getPetDetails"):
        pet_id = dict['pet_id']
        selector = {'_id': {'$eq': pet_id}}
        docs = myDatabase.get_query_result(selector)
    
        pet_doc = None
        for doc in docs:
            if(doc['_id'] == pet_id):
                pet_doc = doc
        pet_details = pet_doc['name']+" is a "+pet_doc['age']+" old "+pet_doc['breed']+". "+pet_doc['comments']
        return {"output":pet_details,"image_url":pet_doc['image_url']}

```
*This code connects to the Cloudant and retrieves the pet details. Depending on the parametes passed, it either retrieves all the pets of one kind or details of one specific pet.*

<details ><summary style="background-color:yellow">Click here for instructions to test the cloud function </summary>
Click on `Invoke with Parameters` and include the following JSON.

```json
{
    "req":"getAllDetails",
    "kind":"Dog"
}
```
Click on `Invoke`. You should see the output on the right side
</details>

4. Enable web-action for the function you just created. 

<img src="images/enable_webaction.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

5. Now that web-action is enabled, we create an API end-point that points to this function. Click on `Create API`.

<img src="images/create_api.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

6. Give it an `API name`. The base path for the API is automaticaly populated. Then click on `Create Operation`.

<img src="images/define_api_endpoint.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

7. A new window pops up where you can specify the path, method and the action you want to map the API to. Enter the details and click on `Create`.

<img src="images/endpoint_method_create.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

8. Now that the operation is created and you are able to see the newly created operation under the Operations list, you just need to scroll down to the bottom of the page, leaving the remaining values as default and click on `Create` to create an API end-point.

<img src="images/create_api.PNG" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

> <span title="camera">📷</span> **Take a screenshot of the API service page showing your newly created API and save it as **'functions-api'** in .jpg or .png format for the peer-graded assignment.**

### Task 3 - Create Watson Assistant that uses the API end points from the cloud function and the web application.

1. Now go to your [dashboard](https://cloud.ibm.com/resources), and click on your **Watson Assistant** service which you have already created.
 
<img src="images/0.1.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

2. Click on the Launch Watson Assistant button to access the web application that will allow you to create chatbots.

<img src="images/0.2.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

3. Click on the Assistants icon in the top left corner as shown in figure.

<img src="images/0.3.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

4. Now, you can provide any name to your assistant and click, Create assistant.

5. To add a dialog skill click on Add an actions or dialog skill.

<img src="images/1.PNG" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

6. Right clik [here](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-CB0106EN-SkillsNetwork/labs/FinalAssignment-coursera/data/PetShop-Customer-Care-Skill.json), and choose `Save link as` to save `PetShop-Customer-Care-Skill.json` in your local directory. Go to the **Upload skill** section and upload the JSON.

<img src="images/4.PNG" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

> <span title="camera">📷</span> **Take a screenshot of the newly created Watson Assistant and save it as **'pet_store_assistant'** in .jpg or .png format for the peer-graded assignment.**


7. Now, go to the functions and click on APIs, and copy the route.

<img src="images/5.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

8. Go to your petstore- customer_care_skill chatbot and click on **Options**, then go to Webhook and paste the API route which you copied in the earlier step.

<img src="images/6.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

> <span title="camera">📷</span> **Take a screenshot of the webhook page where the URL is entered and save it as **'webhook_url'** in .jpg or .png format for the peer-graded assignment.(Please mask/blurr the URL part of the webhook_url screenshot before submission)**


9. Now to test your chatbot go to `Try it out` panel, on the top right. For your reference find the below screenshot.

<img src="images/9.PNG" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:50%"/>

<br/>

<img src="images/10.PNG" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:50%"/>


### Task 4 - Integrate the Watson assitant to the website
1. From Watson assistant you created in the previous task, choose `Integrate Web Chat`

<img src="images/begin_chatbot_integration.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

2. In the Web Chat screen that comes up, click on `Create`.

<img src="images/create_integration.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

3. Click on `Add an Avatar Image` to add a face to your virtual assitant. 
<img src="images/add_avatar_image.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

4. In the space provided for link, include a image URL as per the required dimensions or use [https://github.com/ibm-developer-skills-network/tilsj-petshop_webapp/raw/master/PetShoppePatty.png] and click on `Save`.
<img src="images/link_to_avatar_image.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

5. Change the name of the Virtual Assistant.

6. Change the accent colour to something you like for your chatbot.
<img src="images/accent_colour.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

7. Go to the 'Home screen` tab and add the initial dialog and converstion starters.<img src="images/conversation_starters.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

8. Click on the **embed** tab to view the integration script and copy it. 
<img src="images/copy_embed_script.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

9. Click on `Save and Exit` for the changes to be stored. 
<img src="images/save_and_exit.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>


10. Right click here on [index.html](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-CB0106EN-SkillsNetwork/labs/FinalAssignment-coursera/data/index.html) and download it to your local machine.

11. Open index.html with notepad or other editors on you local machine.
    - Paste the **embed** script you copied in step 3, in the space provided in index.html.
    <img src="images/edit_indexhtml.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

    - Save the file
    
> <span title="camera">📷</span> **Take a screenshot of the index.html page with the changes and save it as **'edited_index.html'** in .jpg or .png format for the peer-graded assignment.(Please mask/blurr the **integrationID** and **serviceInstanceID** in the screenshot before submission)**


12. Open `index.html` in the browser by double-clicking on the html file from the file folder.


<img src="images/integrated chat.png" style="margin-top:10px;margin-bottom:10px;border:solid 1px grey; display:block; margin-left:auto;margin-right:auto;width:75%"/>

> <span title="camera">📷</span> **Load the homepage and take a screenshot of the same showing the chatbot at the bottom right and save it as **'integrated_chat'** in .jpg or .png format for the peer-graded assignment.**

### Task 5 - Test the chatbot

Open the deployment URL and test the chatbot for the following: 

1. Retrieve a list of cats.

> <span title="camera">📷</span> **Take a screenshot of the chatbot showing the list of cats and save it as **'cats_list'** in .jpg or .png format for the peer-graded assignment.**

2. Retrieve a list of dogs.

> <span title="camera">📷</span> **Take a screenshot of the chatbot showing the list of dogs and save it as **'dogs_list'** in .jpg or .png format for the peer-graded assignment.**

3. Retrieve image and details of one selected pet.

> <span title="camera">📷</span> **Take a screenshot of the chatbot showing the details of the selected pet and save it as **'selected_pet_details'** in .jpg or .png format for the peer-graded assignment..**

# Congratulations!
You have completed the tasks for this project. In the peer assignement that follows, you will be required to upload the screenshots you saved in this lab.

## Authors
Lavanya

## Other Contributors
Shubham Yadhav

## Change Log

| Date (YYYY-MM-DD) | Version | Changed By        | Change Description                 |
| ----------------- | ------- | ----------------- | ---------------------------------- |
| 2021-06-25        | 1.0     | Lavanya | Created initial version of the lab |

 Copyright © 2020 IBM Corporation. All rights reserved.

