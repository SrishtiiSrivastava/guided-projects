## Lab : Watson Assistant Chatbot Integration with WhatsApp

Objective for Exercise:
- How to deploy your Watson-powered chatbot to Whatsapp for your business using Twilio.

Integrate your chatbot with WhatsApp messaging so your assistant can exchange messages with the customers. You can automate the communication by deploying your Watson-powered chatbot to WhatsApp for your business. Even if you are not present, you will be able to reply to client inquiries quickly.

This integration creates a connection between your assistant and WhatsApp by using Twilio as a provider.

## Prerequisites:
1. You will need an IBM Cloud account to do this lab. If you have not created one already, click on this [link](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-CC0100EN-SkillsNetwork/labs/IBMCloud_accountCreation/CreateIBMCloudAccount.md.html) and follow the instructions to create an IBM Cloud account.

2. You should have a Watson Assistant service created in your IBM Cloud account. If you have not created one already, click on this [link](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-CB0106EN-SkillsNetwork/labs/Module%201/Create_Watson_Assistant.md.html?origin=www.coursera.org) and follow the instructions to create a Watson Assistant service. 
 

## Task 1: Uploading skill on Watson Assistant

Here, we have provided an example of Flower Shop service skill on Watson Assistant. You can download the JSON using this [link](data/Flower-Shop-Skill.json)

>Note: In case if you don't have a Flower Shop skill created in your Watson Assistant, you can follow the instructions given under Task 1 or else you can skip to Task 2, directly.

1. On your Watson Assistant, click on _**Create Assistant**_.

![](images/1.png)

2. On the Create Assistant page, add a name to your assistant, like Flower Shop Assistant and click _**Create Assistant**_.

![](images/2.2.png)

3. Once your assistant is created, click on _**Add dialog skill**_, to add a skill to your assistant.

![](images/2.png)

4. Click on _**Upload skill**_.

![](images/4.png)

5. Download the [Flower shop.json](data/Flower-Shop-Skill.json) file on your PC and upload it by clicking on _**Click here to select a file**_.

![](images/5.png)

6. After uploading, you will see dialog box of Flower Shop skill.

![](images/7.png)
 
Now, you have successfully uploaded the Flower Shop skill.

## Task 2: Twilio Signup

We will be using Twilio's sandbox to show how this integration works to build and have a conversation with a chatbot through WhatsApp.

1. Click on [Twilio Signup link](https://www.twilio.com/try-twilio?promo=jO1067) and signup with your details.

![](images/start_your_free_trial.png)

2. Verify your email.

![](images/verify_your_email.png)

3. Verify your phone number.

![](images/verify_your%20number.png)

4. Add configuration as suggested below and Click on _**Get started with Twilio**_ 

![](images/set_configuration.png)

## Task 3. Integration with Twillio on your Whatsapp

1. On your [Watson Assistant page](https://eu-de.assistant.watson.cloud.ibm.com/crn%3Av1%3Abluemix%3Apublic%3Aconversation%3Aeu-de%3Aa%2F563ee29a43d1413bb281e9aa78cce1c8%3Ae9127ee5-e3f6-4572-8786-2bf01eeec0fa%3A%3A/home), click _**Flower Shop Assistant**_.

![](images/Click_on_Flower_shop_assistant.png)

2. Under Integrations on the right side, Click on _**Add Integration**_.

![](images/add_integration.png)

3. Under third party integration, Click on _**WatsApp with Twillio**_.

![](images/Watsapp_with_twillio.png)

4. Click _**Create**_.

![](images/Click_create_on_twillio.png)

5. Once you setup your [Twilio account](https://www.twilio.com/login), Go to search bar and search for _**Console Dashboard**_. 

![](images/Console_dashboard.png)

6. Copy **Account SID** and **Auth Token**, as you'll be asked to paste these while setting up Twilio on Watson Assistant (Click on Show to reveal the token).

![](images/IDS.png)

7. Go back to your Watson Assistant and fill **Account SID** and **Auth token** with the values that you got from Twilio Dashboard. A **Webhook URL** generated in the WhatsApp Webhook field. Copy this URL and go back to Twilio. Please make a note of it as it will be used in coming steps.

![](images/web_hook.png)

8. Now we need to set up and configure our Twilio sandbox to integrate it with Watson Assistant.Go to Twillio account. On the left side go to **Messaging** from the expanded menu and select _**Send a WhatsApp message**_ under the **Try it out section**. A popup will appear to activate your Sandbox, select the checkbox whcih says to agree and then Click on _**Confirm**_.

![](images/sandbox_check_in.png)

9. We will be using this testing sandbox for our integration. In **Learn: Twilio Sandbox for WhatsApp** page, Send the given code/message to the number provided (Save the number first) by Twilio from your WhatsApp.

![](images/send%20code.png)

10. After sending the code, your WhatsApp screen should look like this.

![](images/send_code_on_watsapp.png)

11. Once it’s done, you should see **Message Received!** on Twilio like this image. This means that now your phone number is connected to this Twilio-WhatsApp sandbox. Then, click on _**Next: Send a One-Way Message**_. This screen lets you set a template if you’re working with a one-way message such as appointment reminders, order notifications, or verification codes. 

![](images/one_way_message.png)

12. In these instances, only the service is talking to the user. So click on _**Next: Two-Way Messaging**_ to go to the next step.

![](images/two_way_message.png)

13. Then, Click on _**Next: Configure your Sandbox**_ 

![](images/configure_your_sandbox.png)

14. Replace the copied **webhook URL** that you have obtained earlier above from Watson Assistant. You should see your number in the sandbox participants and others can enter this sandbox by sending the code mentioned to the sandbox WhatsApp number (In the below image the code is join climb-solar).  

![](images/webhook_paste.png)

15. Once you’re done then, scroll down the same page and Click _**Save**_.

![](images/webhook_paste-save.png)

16. Now, the Watson assistant is integrated with WhatsApp through Twilio. From your device, send a WhatsApp message to the WhatsApp sandbox number, and you receive the assistant’s response. Go ahead and start testing the chatbot you designated.

![](images/Testing.png)



## Author(s)
[Ratima Raj Singh](www.linkedin.com/in/ratima-raj-singh-4a7001191)

## Contributor(s)
[Srishti Srivastava](linkedin.com/in/srishti-srivastava-343095a8)

## Changelog
| Date | Version| Changed by| Change Description|
|------|--------|--------|---------|
|03-02-2022  |1.1 |Ratima Raj Singh | Created Lab |

