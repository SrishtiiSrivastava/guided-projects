<img src="images/IDSNlogo.png" width="300"/>


# Lab 5: Adding Speech Services

Objective for Exercise:
The idea behind this lab is to show you how to interface with different Watson services, specifically leveraging the Python SDK. Before we begin, create a new Speech to Text service, and  Translator from within IBM Cloud.

### Prerequisites:

1. Watson Assistant with the sample Customer Service Skill JSON loaded.
2. Speech To Text Service
3. Text To Speech Service

### Project Scenario
You are an AI specialist and you are entrusted with the responsibility of building a voice enabled chatbot. You are building amarkey viable product to see the feasibility. In this product:

* We will be posting a query to the chatbot through an audio file (mp3/wav). 

* This audio will then be converted to text by the SpeechToText service. 

* The text returned by the SpeechToText service will be passed to the Watson Assistant.

* The response from Watson Assistant is in the form of a JSON

* We extract the text from the JSON and the convert that into speech

* The speech is rendered to the client through an embedded audio tag

* The dialog can be continuous as we create a session with Watson Assistant and maintain it. 

### Task 1: Add Customer Care skill on Watson Assistant:
1. On your Watson Assistant page, click on **Create Assistant** 

    <img src="images/1Create_Assistant.png" style="border: solid 1px grey"/>

2. On the Create Assistant page, add a name to your assistant, like **Customer Care Assistant**

    <img src="images/2CustomerCare.PNG" style="border: solid 1px grey"/>

3. Once your assistant is created, click on **Add an actions or dialog skill**, to add a skill to your assistant:

      <img src="images/3Addskill.png" style="border: solid 1px grey"/>

4. Click on **Use sample skill** and select **Customer Case Sample Skill**

    <img src="images/4Sampleskill.png" style="border: solid 1px grey"/>

> To get your Assistant ID, you need to go to your **Assistant settings** 
> <img src="images/5AssistantSettings.png" style="border: solid 1px grey"/>

> <img src="images/6AssistantAPI.png" style="border: solid 1px grey"/>

Now, you have successfully loaded the Customer Care sample skill.

### Task 2
1. Download the [zip file](https://github.com/ibm-developer-skills-network/cb106-AIWatsonLab/raw/master/audio_dialogs.zip) in your local folder and extract it. It has a few sample dialogs you can upload and get response for.

1. The code has been done for you already. 
```
git clone https://github.com/ibm-developer-skills-network/cb106-AIWatsonLab.git
```
{: codeblock}

```
cd cb106-AIWatsonLab
```
{: codeblock}

We need to import the right modules for this lab. All the modules have been included in the `requirements.txt`. In the file explorer look through the requirement to check the same.

   
```
pip3 install -r requirements.txt
```
{: codeblock}


You will see .env in the ibmservices folder. Fill up the values for the API keys from the services you have created.

> Note: Pass the values within double quotes.

```
tts_api="The TextToSpeech API Key"
tts_url="The TextToSpeech URL"
stt_api="The SpeechToText API Key"
stt_url="The SpeechToText URL"
assistant_api="The Watson Assistant API Key"
assistant_url="The Watson Assistant URL"
assistant_id="The Watson Assistant Id"
```
> To get your TextToSpeech service API Key and URL, open the TestToSpeech service from your IBM Cloud **Resource list**:
 
> <img src="images/TTS.png" style="border: solid 1px grey"/>

> Similarly, to get your SpeechToText service API Key and URL, open the SpeechToText service from your IBM Cloud **Resource list**:

> <img src="images/STT.png" style="border: solid 1px grey"/>

> Similarly, to get your Watson Assistant service API Key and URL, open the Watson Assistant service from your IBM Cloud **Resource list**:

> <img src="images/WA_API.png" style="border: solid 1px grey"/>

> The steps to get your Assistant ID, has been given in Task 1.


Start the server.
```
python3 server.py
```

Click on `Launch Application` and connect to port `8080`.

You should see this page open up.

<img src="images/request.png" style="border: solid 1px grey"/>

You may recall the flow of the question and response as explained in the beginning of this document. 

<img src="images/FlowOfDialog.png" style="border: 1px solid grey"/>

You can record some mp3/wav files with the questions that are supported by your chatbot. Some questions have been provided in the zip file. You can upload the dialogs and listen to the response that's returned. 

The response is returned in mp3 format. 

<img src="images/response.png" style="border: solid 1px grey"/>

### (Optional) Deploy the application in IBM Cloud

>The deployment will work only if the Cloud Foundry service is available. 

1. Let's sign into IBM Cloud in the terminal using the following command:

```bash
ibmcloud login -u email
```
{: codeblock}

>Replace `email` with the email you used to sign up for IBM Cloud. You will be asked for password; type your password and you will be logged in.

2. Set the resource-group to `Default`.

```
ibmcloud target -g Default
```
{: codeblock}

3. Run the following command to get details of your IBM cloud account.

```
ibmcloud account orgs
```
{: codeblock}

4. The next step is to target CloudFoundry as follows:

```
ibmcloud target --cf-api https://api.REGION.cf.cloud.ibm.com -r REGION -o ACCOUNTOWNER
```
{: codeblock}

> The REGION would be eu-gb for London, us-south for Dallas or other regions depending where you have signed infrom. The ACCOUNTOWNER is your email id.

6. If you are running this code for the first time and you don't have an account space created, run the following command to create an account space. It has been named chatbot_withdialogs. You can give it any name you want. 

```
ibmcloud account space-create chatbot_withdialogs
```

7. We should now set the space that we just created as the target space for our application deployment.

```
ibmcloud target -s chatbot_withdialogs
```

8. Before we deploy the application, let's change the name to something unique across IBM in the `manifest.yml` file. 

```
---
applications:
 - name: <uniquename>_chatbotwithdialogs_app
   random-random: true
   memory: 64M
```

9. We can now deploy our application with `ibmcloud ` as follows. Make sure you are in the directory where the `manifest.yml` file is present.

```bash
ibmcloud app push
```
{: codeblock}

The application would be deployed and you will see and output similar to what you see in the image below. Copy the URL of your application. 

> **Trouble Shooting** - If there is any error in deploying the run `ibmcloud cf logs <your_app_name> --recent` and check what's wrong and then make correction and repeat the process.

**Congratulations!** We hope you've deployed an application to Cloud Foundry on IBM Cloud using the command line interface.






## Author(s)
Lavanya

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2021-06-16 | 1.0 | Lavanya | Create new Lab |
|   |   |   |   |
|   |   |   |   |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>



